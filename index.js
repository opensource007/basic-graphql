var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');

var courses = [
    {id: 1, title: 'Nodejs Course'},
    {id: 2, title: 'PHP Course'},
];

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    course(id: Int!):[Course]
  }
  type Course {
      id: Int
      title: String
  }
`);
 
var getCourse = (args)=>{
    var id = args.id;
    console.log(id);
    return courses.filter(item => item.id === id);
} 

// The root provides a resolver function for each API endpoint
var root = {
    course: getCourse, 
};
 
var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at http://localhost:4000/graphql');